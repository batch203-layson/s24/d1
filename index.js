// console.log("Hello World");

// ES6 is also known as ECMAScript 2015
// ECMAScript is the standard that is used to create implementations of the language, one which is JavaScript.
	// where all browser vendor can implement (Apple, Google, Microsoft, Mozilla, etc.)
// New features to JavasScript

// [SECTION] Exponent Operator

// Using Math object methods
const firstNum = Math.pow(8, 3);
console.log(firstNum);

// Using the exponent operator
const secondNum = 8 ** 3;
console.log(secondNum);

// [SECTION] Template Literals
/*
	- Allows to write string without using the concatenation operator (+).
	- ${} is called placeholder when using template literals, and we can input variables or expression.

*/
let name = "John";

// Hello John! Welcome to Programming!
// Pre-Template Literals
let message = "Hello " + name +"! Welcome to programming!";
console.log("Message without template literals:");
console.log(message);

// Strings using template literals
// Uses backticks(``) instead of ("") or ('')
message = `Hello ${name}! Welcome to programming!`;
console.log("Message with template literals:");
console.log(message);

const anotherMessage = `${name} attended a math competition.
He won it by solving the problem "8 ** 2" with the solution of ${8**2}`;

console.log(anotherMessage);

// [SECTION] Array Destructuring
// It allows us to name array elements with variableNames instead of using the index numbers.
/*
	- Syntax:
		let/const [variableName1, variableName2, variableName3] = arrayName;
*/
const fullName = ["Juan", "Dela", "Cruz"];

// Pre-Array Destructuring
console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);

// Hello Juan Dela Cruz! It's nice to meet you
console.log(`Hello ${fullName[0]} ${fullName[1]} ${fullName[2]}! It's nice to meet you!`);

// Array Destructuring
	// variable naming for array destructuring is based on the developers choice.
const [firstName, middleName, lastName] = fullName;

console.log(firstName);
console.log(middleName);
console.log(lastName);

console.log(`Hello ${firstName} ${middleName} ${lastName}! It's nice to meet you!`);

// [SECTION] Object Destructuring
/*
	- Shortens the syntax for accessing properties form object
	- The difference with array destructuring, this should be exact property name.
	-Syntax:
		let/const {propertyNameA, propertyNameB, propertyNameC} = object;

*/
const person = {
	givenName: "Jane",
	maidenName: "Dela",
	surName: "Cruz"
}

// Pre-object destructuring
console.log(person.givenName);
console.log(person.maidenName);
console.log(person.surName);

console.log(`Hello ${person.givenName} ${person.maidenName} ${person.surName}! It's good to see you again.`);

// Object Destructuring
// const {givenName, maidenName, surName} = person;
// console.log(givenName);
// console.log(maidenName);
// console.log(surName);

// console.log(`Hello ${givenName} ${maidenName} ${surName}! It's good to see you again.`);
					// Object destructuring can also be done inside the parameter.
function getFullName({givenName, maidenName, surName}){
	console.log(`Hello ${givenName} ${maidenName} ${surName}!`);
}

getFullName(person);

// [SECTION] Arrow Functions
